import java.awt.*;
import java.awt.event.*;
import java.io.*;

public class NotepadDemo extends Frame{
	public File openedFile;
	public TextArea ta;
	
	public NotepadDemo(){
		super("Untitled-Notepad");
		setSize(600,500);
		setVisible(true);

		ta = new TextArea();
		add(ta);

		MyMenuBar mb = new MyMenuBar(this);
		setMenuBar(mb);

		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				// System.exit(1);
				dispose();
			}
		});
	}

	public static void main(String args[]){
		new NotepadDemo();
	}
}