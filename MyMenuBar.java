import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class MyMenuBar extends MenuBar{
	public MyMenuBar(NotepadDemo obj){
		// MenuBar mbar = new MenuBar();
		Menu fileMenu = new Menu("File");
		Menu editMenu = new Menu("Edit");
		
		super.add(fileMenu);
		super.add(editMenu);
		
		MenuShortcut shortcutForOpen = new MenuShortcut(KeyEvent.VK_O, false);
		MenuShortcut shortcutForNewWindow = new MenuShortcut(KeyEvent.VK_N, true);
		MenuShortcut shortcutForSave = new MenuShortcut(KeyEvent.VK_S, false);
		MenuShortcut shortcutForFind = new MenuShortcut(KeyEvent.VK_F, false);
		MenuShortcut shortcutForReplace = new MenuShortcut(KeyEvent.VK_H, false);

		MenuItem new1 = new MenuItem("New Window", shortcutForNewWindow);
		MenuItem open = new MenuItem("Open", shortcutForOpen);
		MenuItem save = new MenuItem("Save", shortcutForSave);
		MenuItem exit = new MenuItem("Exit");
		MenuItem find = new MenuItem("Find", shortcutForFind);
		MenuItem replace = new MenuItem("Replace",shortcutForReplace);

		fileMenu.add(new1);
		fileMenu.add(open);
		fileMenu.add(save);
		fileMenu.add(exit);
		editMenu.add(find);
		editMenu.add(replace);

		open.addActionListener(new OpenClickListener(obj));	
		save.addActionListener(new SaveClickListener(obj));
		new1.addActionListener(new NewClickListener(obj));

		find.addActionListener(new FindClickListener(obj));
	}
	
}