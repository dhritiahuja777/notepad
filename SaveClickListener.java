import java.io.*;
import java.awt.*;
import java.awt.event.*;

class SaveClickListener implements ActionListener{
	NotepadDemo obj;
	String src;
	File f;

	public SaveClickListener(NotepadDemo obj){
		this.obj = obj;
	}
	
	public void actionPerformed(ActionEvent ae){
		if(this.obj.openedFile == null){
			//case1: if we open a new file and then save it!
			String textContent;
			textContent = obj.ta.getText();
			FileDialog fd = new FileDialog(obj,"Save as",FileDialog.SAVE);
			fd.setVisible(true);
			if(fd.getDirectory()!=null && fd.getFile()!=null){
				src = fd.getDirectory() + fd.getFile();
			}
			try{
				this.obj.openedFile = new File(src);
				f = new File(src);
				FileOutputStream fout = new FileOutputStream(f,false);
				byte b[];
				b=textContent.getBytes();
				fout.write(b);
				obj.setTitle(fd.getFile());
			} catch(IOException ie){
				System.out.println("IOException caught");
			} catch(NullPointerException npe){
				System.out.println("NPE caught");
			}
		}
		else{
			//case2: if we open an existing file and then save it!
			String textContent;
			textContent = obj.ta.getText();
			try{
				FileOutputStream fout = new FileOutputStream(this.obj.openedFile,false);
				byte b[];
				b=textContent.getBytes();
				fout.write(b);
			} catch(IOException ie){
				System.out.println("IOException caught");
			} catch(NullPointerException npe){
				System.out.println("Npe");
			}
		}	
	}
}