import java.io.*;
import java.awt.*;
import java.awt.event.*;

class OpenClickListener implements ActionListener{
	NotepadDemo obj;

	String src;
	String line;
	String actualContent;


	public OpenClickListener(NotepadDemo obj){
		this.obj = obj;
	}

	public void actionPerformed(ActionEvent ae){
		actualContent = "";
		FileDialog fd = new FileDialog(obj,"Open",FileDialog.LOAD);
		fd.setVisible(true);
		if(fd.getDirectory()!=null && fd.getFile()!=null){
			src = fd.getDirectory() + fd.getFile();
		}
		try{
			this.obj.openedFile = new File(src);
			File f = new File(src);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			obj.setTitle(fd.getFile());
			while((line=br.readLine())!=null){
				actualContent+= line + "\n";
			}
			// System.out.println(actualContent);
			obj.ta.setText(actualContent);

			br.close();
		} catch(FileNotFoundException e){
			System.out.println("Caught file not found");
		} catch(IOException e){
			System.out.println("IOException is caught");
		} catch(NullPointerException npe){
			System.out.println("NPE caught");
		}
	}
}