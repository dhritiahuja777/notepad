import java.awt.*;
import java.awt.event.*;

class FindClickListener implements ActionListener{
	NotepadDemo obj;

	public FindClickListener(NotepadDemo obj){
		this.obj = obj;
	}
	
	public void actionPerformed(ActionEvent ae){
		Dialog dialog = new Dialog(obj,"Enter the text to be searched!",false);
		dialog.setLayout(new FlowLayout());
		dialog.setSize(250,100);

		TextField textField = new TextField(25);

		textField.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				String key = textField.getText();
				int startPos = obj.ta.getText().indexOf(key);
				if(startPos >=0){
					//required text found!
					int endPos = startPos + key.length();
					System.out.println(startPos);
					obj.ta.requestFocus();
					obj.ta.setSelectionStart(startPos);
					obj.ta.setSelectionEnd(endPos);
				}else{
					//required text not found!
					Dialog notFoundDialog = new Dialog(obj, "Notepad", true);
					notFoundDialog.setLayout(new GridLayout(2,1));
					notFoundDialog.setSize(150,150);

					Label notFoundLbl = new Label("Cannot find " + key);

					Panel okPnl = new Panel();
					Button okBtn = new Button("OK");
					okBtn.setBackground(Color.BLUE);
					okBtn.setForeground(Color.BLACK);
					okBtn.setSize(100,100);
					okPnl.add(okBtn);

					okBtn.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent ae){
							notFoundDialog.dispose();
						}
					});

					notFoundDialog.add(notFoundLbl);
					notFoundDialog.add(okPnl);

					notFoundDialog.addWindowListener(new WindowAdapter(){
						public void windowClosing(WindowEvent we){
							notFoundDialog.dispose();
						}
					});

					notFoundDialog.setVisible(true);
				}
			}
		});

		dialog.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				dialog.dispose();
			}
		});

		dialog.add(textField);
		dialog.setVisible(true);

	}

}